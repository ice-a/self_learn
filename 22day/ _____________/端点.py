from flask import Flask,url_for
app = Flask("myapp")
@app.route("/")
def index():
    url1 = url_for('say_hello',name='Gavin')
    url2 = url_for('say_hi',name='Gavin')
    url3 = url_for('hello',name='default')
    return "url1 is {},<br> url2 is {}<br> url3 is {}.".format(url1,url2,url3)
@app.route("/hi/<name>",endpoint='say_hi')
@app.route("/greeting/<name>")
def hello(name):
    return 'Hello,{0}'.format(name)
app.add_url_rule('/hello/<name>', view_func='hello',endpoint='say_hello')
app.run()