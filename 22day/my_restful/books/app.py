from flask import Flask
def create_app(app_name='BOOK_API'):
	app=Flask(app_name)
	app.config.from_object('books.config')
	from books.api import api 
	app.register_blueprint(api,url_prefix='/api')
	from books.models import db
	db.init_app(app)
	return app
