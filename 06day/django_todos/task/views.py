from django.shortcuts import render, redirect
from django.utils import timezone
from django.http import HttpResponse
from .models import Task

from django.contrib.auth.decorators import login_required


@login_required
def home(request):
    """
    首页
    """
    time = timezone.localtime(timezone.now())
    return render(request, template_name='task/home.html',
                  context={'nowtime': time})

# @login_required
def todo_list(request):
    """
    返回所有todo list。
    """
    time = timezone.localtime(timezone.now())
    if request.method == 'GET':
        task = Task.objects.filter(user_id_id=request.user.id)
        return render(request, template_name='task/todos.html',
                      context={'todos': task, 'nowtime': time})

# @login_required
def todo_create(request):
    """
    创建一个todo
    """
    if request.method == 'GET':
        return render(request,template_name='task/create.html')
    title = request.POST.get('title')
    
    Task.objects.create(title=title,user_id_id=request.user.id)  # **task 传给  **kwargs, 绝配
    return redirect('tdlist')
   

@login_required
def todo_delete(request,pk):
    try:
        task = Task.objects.get(pk=pk)
        task.delete()
    except Exception as e:
        print('删除失败！')
    return redirect('tdlist')


def todo_update(request,pk):

    task = Task.objects.get(pk=pk)
    task.completed = not task.completed
    task.save()
    return redirect('tdlist')
# ----------------------------------------------------------