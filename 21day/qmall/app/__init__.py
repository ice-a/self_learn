from flask import Flask
#pip install flask_sqlalchemy
from flask_sqlalchemy import SQLAlchemy

app=Flask(__name__)

# 加载配置信息
app.config.from_object('config')\
# 定义(创建)数据库对象
db=SQLAlchemy(app)

# 注册
from app.module_a.controllers import module_a as auth_module
app.register_blueprint(auth_module)


# 构建数据库
db.create_all()