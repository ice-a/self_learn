from  flask import Flask,url_for,render_template

app=Flask(__name__,static_folder='papg')
app.debug=True

@app.route('/test_static')
def test_static():
    url=url_for('static',filename='1.jpg')
    print('url:{}'.format(url))
    return render_template('img.html')
    # return 'url:{}'.format(url)

if __name__=='__main__':
    app.run()