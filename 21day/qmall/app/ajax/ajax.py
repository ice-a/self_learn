from flask import Flask
from flask import jsonify,request,render_template
app=Flask(__name__)
app.debug=True


@app.route('/add')
def add():
    print('_____________________________________-')
    # 从前端接收两个数据，把两个数相加
    a=request.args.get('a')
    b=request.args.get('b')
    print(a)
    result=int(a)+int(b)
    data=jsonify(result=result)
    # result={
    #     'result':result
    # }
    return data

@app.route('/')
def index():
    return render_template('layout.html')
if __name__=='__main__':
    app.run()