#导入form类
from flask_wtf import Form

#导入表单用到的字段类型
from wtforms import StringField,PasswordField
#导入验证form验证器
from wtforms.validators import  DataRequired,Email

class UserForm(Form):
    # 姓名字段
    name=StringField('Name')
    # 邮箱字段
    email=StringField('邮箱地址',[Email(),DataRequired(message='请输入邮箱')])
    # 密码字段

    password=PasswordField('密码',[DataRequired(message='亲,你没输入密码，请输入密码！')])
class LoginForm(Form):
    # 邮箱字段
    email = StringField('邮箱地址',[Email(),DataRequired(message='邮箱地址未提供!')])
    # 密码字段
    password = PasswordField('密码',[DataRequired(message='请输入密码!')])