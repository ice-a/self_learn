from flask import Flask,flash,render_template,request,url_for,redirect
app=Flask(__name__)
app.secret_key='123asdfgbc'
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login',methods=['GET','POST'])
def login():
    error=None
    if request.method=='POST':
        username=request.form.get('username')
        if username=='emily':
            flash('sucess!')
            return redirect(url_for('index'))
        else:
            error='失败'
    return render_template('login.html',error=error)
if __name__=='__main__':
    app.run()

