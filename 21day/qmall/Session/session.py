'''
Cookie和Session结合使用
1,把Session存储在服务端，把session对应的id存储在Cookie中
如果用户已经登录，客户端把session_id发给服务器，服务器根据session_id从数据库找到对应的session(jango的方法)
2,把session加密，直接存储在cooke(flask的方法)
'''



from flask import Flask, session, request, redirect, url_for

app=Flask(__name__)
app.debug=True
app.secret_key='13234ghjkl;dcvbn'


@app.route('/')
def index():
    if 'username' in session:
        return 'welcome {}'.format(session['username'])
    return '你没有登录'
@app.route('/login',methods=['GET','POST'])
def login():
    if request.method=='POST':
        session['username']=request.form['username']
        return redirect(url_for('index'))

    return '''
    <form action='/login' method='POST'>
        <input type='text' name='username'>
        <input type='submit' name='Login' value='dlu'>
    </form>
        
    '''
@app.route('/logout')
def logout():
    # 删除保存在session中的登录信息
    session.pop('username')
    return redirect(url_for('index'))
app.run(host='0.0.0.0')