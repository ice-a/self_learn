from django.http import HttpResponseRedirect
from web.helpers.utils import multi_encrypt
from web.models import Composer
import re



need_login='(/show_list/[1-9]\d{1,}/)'
class AuthMiddleware(object):
    def __init__(self,get_response):
        self.get_response=get_response

    def __call__(self, request):

            auth=request.COOKIES.get('Authorization')
            cid=request.COOKIES.get('cid')
            composer=Composer.objects.filter(cid=cid).first()


            if composer and auth ==multi_encrypt(composer.cid,composer.phone):
                request.composer=composer
            elif re.findall(need_login,request.path):

                return HttpResponseRedirect('/login/')
            response=self.get_response(request)
            return response
