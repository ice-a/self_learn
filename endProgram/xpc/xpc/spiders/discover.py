# -*- coding: utf-8 -*-
import scrapy
import json
from scrapy import Request
from xpc.items import PostItem,CommentItem,ComposerItem,CrItem
def cover_int(s):
    if not s:
        return 0
    return int(s.replace(',',''))
class DiscoverSpider(scrapy.Spider):
    name = 'discover'
    allowed_domains = ['xinpianchang.com']
    start_urls = ['http://www.xinpianchang.com/channel/index/type-0/sort-like/duration_type-0/resolution_type-/page-29']
    def start_requests(self):
        for url in self.start_urls:
            # dont_filter：本次请求不执行过滤重复url的逻辑
            request=Request(url,dont_filter=True)
            request.meta['dont_merge_cookies']=True
            yield request
    def parse(self, response):
        post_url='http://www.xinpianchang.com/a%s?from=ArticlexxList'
        li_list=response.xpath("//ul[@class='video-list']/li")
        #print(len(li_list))
        for li in li_list:
            #视频！id
            p_id=li.xpath('./@data-articleid').get()
           # print(p_id)
            #视频首图片
            p_img=li.xpath("./a/img/@_src").get()
            #视频时长
            p_duration=li.xpath('./a/span/text()').get()
           # print('[[[]]]]]]]',p_duration)
            if p_duration:
                minutes,seconds,*_=p_duration.split("'")
                p_duration=int(minutes)*60+int(seconds)

            #视频详情页面地址
            v_url = (post_url % p_id)

            request=Request(v_url,callback=self.parse_post)
            request.meta['p_id']=p_id
            request.meta['p_img']=p_img
            request.meta['p_turation']=p_duration

            yield request
        other_page=response.xpath('//div[@class="page"]/a/@href').extract()
        for page in other_page:
            request=Request(page,callback=self.parse)
            # 不要合并cookies，这样可以使用settings里设置的cookies
            request.meta['dont_merge_cookie']=True
            yield request
            #print('******************************************',page)



    def parse_post(self,response):


        # 视频详情页进行爬取
        post=PostItem()
        post['p_id']=response.meta['p_id']
        post['p_img'] = response.meta['p_img']
        #播放时长
        post['p_turation'] = response.meta['p_turation']
       # #  #print(response.url)
        post['p_title']=response.xpath("//div[@class='title-wrap']/h3/text()").get()
       # #视频预览图
        post['preview'] = response.xpath("//div[@class='filmplay']//img/@src").get()
       #视频url
        video=response.xpath("//a[@id='player']/@href").get()
        post['p_video'] =video[2:]
        #视频所属类
        cates= response.xpath('//span[contains(@class,"cate")]//text()').extract()
        post['p_category'] = ''.join([cate.strip() for cate in cates])
     #发表时间
        post['create_at'] = response.xpath("//span[contains(@class,'update-time')]/i/text()").get()
       #播放次数
        a=post['play_counts']= cover_int(response.xpath("//div[contains(@class,'filmplay-data-play')]/i/text()").get())
        #被点赞次数
        #print('================',a)
        post['like_counts'] =a=cover_int(response.xpath("//span[contains(@class,'like-counts')]/text()").get())

        yield post


        # 视频制作者
        p_id = response.meta['p_id']
        creater_list=response.xpath('//div[@class="filmplay-creator right-section"]/ul[@class="creator-list"]/li')
        for creater in creater_list:

            cr=CrItem()
            cid=creater.xpath('./a/@data-userid').get()
            cr['pcid']='%s_%s' %(cid,p_id)
            cr['cid']=cid
            cr['pid']=p_id=response.meta['p_id']
            cr['roles']=creater.xpath('.//span[contains(@class, "roles")]/text()').get()
            yield cr
            #print(cr)
        #用户首页地址
        user_url='http://www.xinpianchang.com/u%s?from=articleList'
        request=Request(user_url%cid,callback=self.parse_composer)
        request.meta['cid']=cid
        yield request

        # 评论地址
        # 评论信息的url模板
        comment_url = 'http://www.xinpianchang.com/article/filmplay/ts-getCommentApi?id=%s&ajax=0&page=1'
        # 构造评论接口的request，并返回
        request = Request(comment_url % p_id, callback=self.parse_comment)
        yield request

    def parse_composer(self,response):
        #用户信息`
        composer=ComposerItem()
        composer['cid']=response.meta['cid']
       ## 用户主页的背景大图
        banner=response.xpath('//div[@class="banner-wrap"]/@style').get()
        if banner:
            composer['banner']=banner[21:-1]
        #用户头像
        composer['avatar']=response.xpath('//span[@class="avator-wrap-s"]/img/@src').get()
        # # 用户是否是官方认证用户
        composer['verified'] = response.xpath('//span[@class="avator-wrap-s"]/span/@class').get()
        # 用户名称
        composer['name'] = response.xpath('//div[@class="creator-info"]/p[contains(@class,"creator-name")]/text()').get()
        # 自我介绍
        composer['intro'] = response.xpath(
            '//p[contains(@class, "creator-desc")]/text()').get()
        # 用户被点赞的次数
        composer['like_counts'] = cover_int(response.xpath('//p[contains(@class,"creator-detail")]//span[contains(@class,"like-counts")]/text()').get())
        # 粉丝数量
        composer['fans_counts'] =  cover_int(response.xpath('//p[contains(@class,"creator-detail")]//span[contains(@class,"fans-counts")]/text()').get())
        # 关注数量
        composer['follow_counts'] =  cover_int(response.xpath('//p[contains(@class,"creator-detail")]//span[contains(@class,"follow-wrap")]/span[2]/text()').get())
        # 用户所在地区，定位到icon-location这个span,然后再取它相邻的下一个span
        location =response.xpath('//p[contains(@class,"creator-detail")]//span[contains(@class,"icon-location")]/following-sibling::span[1]/text()').get()
        if location:

            composer['location']=location.strip().replace('\xa0','') or 0
        # 用户的职业，xpath同上
        composer['career'] = response.xpath(
            '//span[contains(@class, "icon-career")]/following-sibling::span[1]/text()').get() or ''
        yield composer
       # print(composer)



        #评论
    def parse_comment(self,response):
        #
        result=json.loads(response.text)
        comments=result['data']['list']
        for c in comments:
            comment=CommentItem()
            #评论内容
            comment['content']=c['content']
            #评论者id
            comment['comment_id']=c['commentid']
            #作品id
            comment['pid']=c['articleid']
            # 评论发表时间
            comment['created_at'] = c['addtime']
            # 评论被点赞的次数
            comment['like_counts'] = c['count_approve']
            # 发表评论的用户ID
            a=c['userInfo']['userid']
            if a:
                comment['cid'] =a
            else:
                comment['cid'] =0
                # 发表评论的用户名称
            comment['uname'] = c['userInfo']['username']
            # 发表评论的用户头像
            comment['avatar'] = c['userInfo']['face']
            # 如果本条评论是回复另一条评论，则reply不为空
            if c['reply']:
                #
                # 把被回复的那条评论ID存在reply字段
                comment['reply'] = c['reply']['commentid']
            yield comment
           # print(comment)
        #是否有下一页

        next_page = result['data']['next_page_url']
        if next_page:
            yield Request(next_page, callback=self.parse_comment)