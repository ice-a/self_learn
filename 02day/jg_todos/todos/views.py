from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def home(request):

	print(request)
	print(dir(request))
	print(type(request))
	print(request.POST)
	print(request.path)
	print("-----------------------------")
	resp=HttpResponse('hello')
	print(resp)
	print('--------------------------------------------------')
	print(resp,type(resp),dir(resp))

	return render(request,'html_list/home.html')
def todos_list(request):

	return render(request,'html_list/list.html')
def sign_in(request):
	return render(request, 'html_list/login.html')