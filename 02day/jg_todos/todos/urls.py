from django.conf.urls import url,include
from todos import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^list/$',views.todos_list),
    url(r'^login$',views.sign_in,name='login')
]
