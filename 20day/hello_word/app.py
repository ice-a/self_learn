
from flask import Flask
from flask import request
from flask import render_template

from werkzeug.routing import BaseConverter
app=Flask(__name__)


app.debug=True
class ReConverter(BaseConverter):
	def __init__(self,url,*args):
		self.url=url
		self.regex=args[0]
app.url_map.converters['re']=ReConverter


@app.route('/test/<re(r"\d+"):page>')
def test_re(page):
	return page


@app.route('/hh')
def hello_word():
	name=request.args.get('name')
	password=request.args.get('password')
	#return name

	return 'hello_word'


@app.route('/login',methods=['GET','POST'])

def login():
	msg=None
	name='emily'
	pwd='123'
	if request.method=='POST':
		username=request.form.get('username')
		password=request.form.get('password')
		if username==name and password==pwd:
			return 'ok'
		msg='sorry!your infomation is wrong'
	return render_template('login.html',msg=msg)




if __name__=='__main__':
	print(app.url_map)
	app.run()
# export FLASK_APP=app.py
 # 1195  flask run
