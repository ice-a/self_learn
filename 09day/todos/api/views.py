from django.http import HttpResponse
from django.core import serializers
from .models import Task
# Create your views here.


def task_a(request):
    """
    使用Django自带的序列化
    """
    data = serializers.serialize("json", Task.objects.all())
    print(data)
    # Default Content-Type: text/html;charset=utf-8
    # content_type="application/json;charset=utf-8"
    # content_type="application/json;charset=utf-8"
    response = HttpResponse(data,content_type="application/json;charset=gbk")
    response['Content-Length'] = len(data)
    return response

def task_b(request):
    """
    自定义json格式。
    """
    import json
    data = {"data":[]}
    tasks = Task.objects.values_list('completed','title')
    for completed,title in tasks:
        item = {'completed':completed,'title':title}
        data["data"].append(item)
    print(data)
    json_data = json.dumps(data)
    response = HttpResponse(json_data,content_type="application/json;charset=utf-8")
    response['Content-Length'] = len(json_data)
    return response