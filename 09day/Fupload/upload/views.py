from django.shortcuts import render
from django.http import HttpResponse
from upload.forms import FileUploadForm

# Create your views here.
from upload.models import FileSimpleModel


def upload_file(request):
    if request.method == 'POST':
        print('1111111111111111111111111111111111')
        my_file = FileUploadForm(request.POST, request.FILES)
        if my_file.is_valid():

            print('22222222222222222222222')
            f = my_file.cleaned_data['my_file']
            handle_uploaded_file(f)
            return HttpResponse('文件上传成功')

    else:
        my_file = FileUploadForm()
    return render(request, 'upload/upload.html', {'form': my_file})


def handle_uploaded_file(f):
    # with open(f.name,'wb+') as destination:
    #     for chunk in f.chunks():
    #         destination.write(chunk)
    file_model = FileSimpleModel()
    file_model.file_field = f
    file_model.save()
