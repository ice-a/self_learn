from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.http import HttpResponse
from django.views import View


def hello(request):
    if request.method == 'GET':
        return HttpResponse('Hello')


class Hello(View):
    def get(self, request):
        return HttpResponse('Hello类视图')


class GreetingView(View):
    greeting = "Good Day"

    def get(self, request):
        return HttpResponse(self.greeting)


class MorningGreetingView(GreetingView):
    greeting = "Morning！"
