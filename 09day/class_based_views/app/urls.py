from django.conf.urls import url
from .views import Hello

urlpatterns = [
    url(r'^hello/$', Hello.as_view()),
]