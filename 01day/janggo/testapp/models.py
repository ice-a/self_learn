from django.db import models

# Create your models here.

class Test(models.Model):
    """
    Model for  storing 'tasks'
    一个Task对应一个数据库中的表
    一个Task的实例对应表中的一条记录
    一个Task中的属性对应表中的一个字段
    """
    # Task content
    content = models.CharField(max_length=100)
    # 任务是否完成,默认值为未完成  Whether this task is completed.
    completed = models.BooleanField(default=False)